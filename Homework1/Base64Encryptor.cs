﻿namespace Homework1
{
    using System.Text;

    /// <summary>
    /// Class to encrypt or decript base64.
    /// </summary>
    public class Base64Encryptor
    {
        /// <summary>
        /// Method to encrypt a string into a base64 code.
        /// </summary>
        /// <param name="text">string to be encoded.</param>
        /// <returns>Base64 code.</returns>
        public static string EncryptBase64(string text)
        {
          byte[] bytes = Encoding.UTF8.GetBytes(text);
          return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// Method to decrypt a base 64 code to a string.
        /// </summary>
        /// <param name="text">Base 64 code to be decoded.</param>
        /// <returns>String decoded.</returns>
        /// <exception cref="FormatException">In case the base64 code is invalid.</exception>
        public static string DecryptBase64(string text)
        {
            string result = string.Empty;

            // comment
            try
            {
                byte[] bytes = Convert.FromBase64String(text);
                result = Encoding.UTF8.GetString(bytes);
            }
            catch (FormatException ex)
            {
                throw new FormatException("Invalid base64 format", ex);
            }

            return result;
        }
    }
}