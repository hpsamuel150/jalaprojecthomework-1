using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework1;
using System;
namespace Homework1.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestBase64Encrypt()
        {
            // Arrange
            string textToEncode = "This a simple text";
            string expectedResult = "VGhpcyBhIHNpbXBsZSB0ZXh0";
            // Act
            string encodedString = Base64Encryptor.EncryptBase64(textToEncode);
            // Assert
            Assert.AreEqual(expectedResult, encodedString);
        }

        [TestMethod]
        public void TestBase64Decrypt()
        {
            // Arrange
            string textToDecode = "VGhpcyBhIHNpbXBsZSB0ZXh0IGJhY2s=";
            string expectedResult = "This a simple text back";
            // Act
            string decodedString = Base64Encryptor.DecryptBase64(textToDecode);
            // Assert
            Assert.AreEqual(expectedResult, decodedString);
        }

        // [TestMethod]
        // public void TestNegativeBase64Decrypt()
        // {
        //     // Arrange
        //     string textToDecode = "VGhpcyBhIHNpbXBs%ZSB%0ZXh0IGJhY2s=";
        //     // Act
        //     var e = Assert.ThrowsException<FormatException>(() => Base64Encryptor.DecryptBase64(textToDecode));
        //     // Assert
        //     Assert.AreEqual(e.Message, "Invalid base64 format");
        // }
    }
}