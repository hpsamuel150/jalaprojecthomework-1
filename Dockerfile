FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as build
WORKDIR /src
COPY . .
RUN dotnet restore "./Homework1.sln"
RUN dotnet build "./Homework1.sln" -c Release -o /app
CMD ["dotnet","test"]


